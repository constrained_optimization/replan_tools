Crawler Python API
==================

Getting started with Crawler is easy.
The main class you need to care about is :class:`~crawler.main.Crawler`

Getting started with Crawler is easy.
The main class you need to care about is :class:`~crawler.main.Crawler`

crawler.main
------------

.. automodule:: crawler.main
   :members:
   :undoc-members:
   :show-inheritance:

crawler.utils
-------------

.. automodule:: crawler.utils
   :members:
   :undoc-members:
   :show-inheritance:

.. testsetup:: *

   from crawler.utils import should_ignore, log

.. automethod:: crawler.utils.should_ignore
.. automethod:: crawler.utils.log
