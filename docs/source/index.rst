   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Testing sphinx documentation!
=================================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. toctree::
   :caption: Programmer Reference

   api
 


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

